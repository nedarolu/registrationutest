# Author: William Alexander Vanegas

@stories
Feature: uTest Page
  As a user, want to create new account at utest.com page

  @scenario1
  Scenario Outline: Registration form
    Given as Alex want to create a new account at utest page

    When he record the data requested on the page
      | firstName      | lastName      |   email      | monthBirth      | dayBirth      | yearBirth      |   city      | postalCode      | country      |  password   |
      | <strfirstName>  | <strlastName>  | <stremail> | <strmonthBirth> | <strdayBirth> | <stryearBirth> | <strcity> | <strpostalCode> | <strcountry> | <strpassword> |

    Then he finish the registration process
      | assertLabel      |
      | <strassertLabel> |

    Examples:
      | strfirstName      | strlastName      | stremail   | strmonthBirth | strdayBirth          | stryearBirth  | strcity  | strpostalCode | strcountry |  strpassword   | strassertLabel |
      | alexander              |     Rojas      | pedritocarlas.rojas@gmail.com |      August     | 8           | 1992         |    Bogotá | 110911      | Colombia  |  TEKt;4nqOC |   Begin uTest Academy |

