package co.com.choucair.utest.stepdefinitions;

import co.com.choucair.utest.model.UTestData;
import co.com.choucair.utest.questions.Answer;
import co.com.choucair.utest.tasks.*;
import co.com.choucair.utest.tasks.JoinToday;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class UTestStepDefinition {

    @Before
    public void setStage(){
        setTheStage(new OnlineCast());
    }

    @Given("^as Alex want to create a new account at utest page$")
    public void asAlexWantToCreateANewAccountAtUtestPage() {

        theActorCalled("Alex").wasAbleTo(OpenBrowserPage.atUTestPage());
    }

    @When("^he record the data requested on the page$")
    public void heRecordTheDataRequestedOnThePage(List<UTestData> data) {
        theActorInTheSpotlight().attemptsTo(
                JoinToday.pressTheOptionJoinToday(),
                PersonalInformation.registerPersonalDataOfTheUser(data),
                YourAddress.registerGeographicLocationData(data),
                YourDevices.registerDeviceSpecificationData(),
                SecurityInformation.AcceptTermsAndConditionsAndPrivacyPolicy(data)
        );
    }

    @Then("^he finish the registration process$")
    public void heFinishTheRegistrationProcess(List<UTestData> label) {
        theActorInTheSpotlight().should(seeThat(Answer.validateLogin(label)));
    }
}
