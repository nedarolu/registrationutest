package co.com.choucair.utest.tasks;

import co.com.choucair.utest.model.UTestData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import org.openqa.selenium.Keys;

import java.util.List;

import static co.com.choucair.utest.userinterface.YourDevices.LAST_STEP;

public class YourDevices implements Task {

    public static YourDevices registerDeviceSpecificationData() {

        return Tasks.instrumented(YourDevices.class);
    }



    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LAST_STEP));
    }
}
