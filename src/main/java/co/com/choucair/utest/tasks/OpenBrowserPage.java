package co.com.choucair.utest.tasks;

import co.com.choucair.utest.userinterface.MainPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class OpenBrowserPage implements Task {
    private MainPage utestPage;

    public static OpenBrowserPage atUTestPage() {

        return Tasks.instrumented(OpenBrowserPage.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(utestPage));
    }
}
