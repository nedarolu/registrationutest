package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SignUpPage {
    public static final Target BEGIN_UTEST_ACADEMY_LABEL= Target.the("label to confirm registration")
            .located(By.xpath("//*[@id='mainContent']/div/div/div[2]/div[2]/div[1]/div[2]/a"));

}
