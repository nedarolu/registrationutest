package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class YourDevices {

    public static final Target LAST_STEP = Target.the("last step")
            .located(By.xpath("//a[@aria-label='Next - final step']"));
}
