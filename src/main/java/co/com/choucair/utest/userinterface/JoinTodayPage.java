package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class JoinTodayPage {
    public static final Target JOIN_TODAY = Target.the("join today")
            .located(By.className("unauthenticated-nav-bar__sign-up"));
}
