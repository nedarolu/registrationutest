package co.com.choucair.utest.questions;

import co.com.choucair.utest.model.UTestData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static co.com.choucair.utest.userinterface.SignUpPage.BEGIN_UTEST_ACADEMY_LABEL;

public class Answer implements Question<Boolean> {
    private final List<UTestData> assertLabel;

    public Answer(List<UTestData> lastStepLabel) {

        this.assertLabel = lastStepLabel;
    }

    public static Answer validateLogin(List<UTestData> lastStepLabel) {
        return new Answer(lastStepLabel);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String labelConfirm = Text.of(BEGIN_UTEST_ACADEMY_LABEL).viewedBy(actor).asString();
        return assertLabel.get(0).getAssertLabel().equals(labelConfirm);
    }
}
